from django.utils.translation import ugettext_lazy as _
from features import Feature


class Demo(Feature):

    name = _('Demo')
    description = _('This is a demo')
    feature_type = 'feature'
    activate_by_default = False
    icon = 'fa fa-bolt'
    category = (_('AGEPoly'), 'fa fa-truck')
    sub_category = (_('Demo'), 'fa fa-truck')

    configs = [
        Feature.Config('test', 'text', '', True, _('This is a test')),
    ]
