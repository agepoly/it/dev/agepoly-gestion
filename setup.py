#!/usr/bin/python

from setuptools import setup

setup(
    name='agepoly-gestion-extra',
    version='0.0.2',
    description='AGEPoly gestion extra states',
    long_description='AGEPoly gestion extra states',
    author='Maximilien Cuony',
    author_email='maximilien.cuony@arcanite.ch',
    url='ssh://git@git.polylan.ch:10622/agep-info/agepoly-gestion-extra.git',
    download_url='https://git.polylan.ch/agep-info/agepoly-gestion-extra',
    classifiers=[
    ],
    packages=[
        'extra_agepoly',
    ],
    include_package_data=True,
    install_requires=[
    ],
)
